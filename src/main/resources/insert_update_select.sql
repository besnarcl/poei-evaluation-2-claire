
-- joins between members, wine and review
SELECT *
FROM wine
         LEFT JOIN review r on wine.id = r.wine_id
         LEFT JOIN members m on r.member_id = m.id AND r.wine_id = wine_id
WHERE wine_id = ?;


INSERT INTO members (firstname, lastname, pseudonym)
VALUES ('Alexandre', 'BARON', 'george-seche')
RETURNING *;

INSERT INTO wine (appellation, region, vintage, colour, variety)
VALUES ('Gigondas', 'Vallee du Rhone', 2019, 'Rouge', 'Grenache')
RETURNING *;

INSERT INTO review ( member_id, wine_id, comment, score)
VALUES (1, 1, 'Bon vin', 5)
RETURNING *;

UPDATE members
SET firstname='Alexandra', lastname='BARRON', pseudonym='sec'
WHERE id =1
RETURNING *;

UPDATE members SET firstname='Loulou'
WHERE id =2
RETURNING *;

SELECT *
FROM members;

SELECT *
FROM members
WHERE id = 2;