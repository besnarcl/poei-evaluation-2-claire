CREATE TABLE IF NOT EXISTS members
(
    id integer GENERATED ALWAYS AS identity PRIMARY KEY ,
    firstname text NOT NULL ,
    lastname text NOT NULL ,
    pseudonym text NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS wine
(
    id integer GENERATED ALWAYS AS identity PRIMARY KEY ,
    appellation text NOT NULL ,
    region text NOT NULL ,
    vintage integer NOT NULL ,
    colour text NOT NULL ,
    variety text NOT NULL
);

CREATE TABLE IF NOT EXISTS review
(
    member_id integer NOT NULL REFERENCES members (id),
    wine_id integer NOT NULL REFERENCES wine (id),
    comment text NOT NULL ,
    score integer NOT NULL,
    PRIMARY KEY ( member_id, wine_id)
    );