package com.zenika.poei.amismaisonduvin.Repository;

import com.zenika.poei.amismaisonduvin.domain.Member;

import java.util.List;

public interface MemberRepository {

    // Create and Update a member
    Member save(Member member);

    // Get all members
    List<Member> getMembers();

    // Get a member by his id
    Member getMemberById(Integer id);

    // Delete member not request in specification
}
