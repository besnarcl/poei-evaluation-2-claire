package com.zenika.poei.amismaisonduvin.Repository;

import com.zenika.poei.amismaisonduvin.domain.Member;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@Primary
public class JdbcMemberRepository implements MemberRepository{

    public static final RowMapper<Member> MEMBER_ROW_MAPPER = (rs, i) -> new Member(
            rs.getInt("id"),
            rs.getString("firstname"),
            rs.getString("lastname"),
            rs.getString("pseudonym"));


    private final JdbcTemplate jdbcTemplate;

    // Constructor
    public JdbcMemberRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // Create a member
    public Member create(Member member){
        return jdbcTemplate.queryForObject("INSERT INTO members (firstname, lastname, pseudonym) VALUES (?,?,?) RETURNING *",
                MEMBER_ROW_MAPPER,
                member.getFirstName(),
                member.getLastName(),
                member.getPseudonym());

    }

    // Update a member
    public Member update(Member member){
        return jdbcTemplate.queryForObject("UPDATE members SET firstname=? WHERE id =?", MEMBER_ROW_MAPPER,
                member.getFirstName(),
                member.getId());
    }

    @Override
    public Member save(Member member) {
        if(member.getId() == null) {
            return create(member);
        }else {
            return update(member);
        }
    }

    @Override
    public List<Member> getMembers() {
        return jdbcTemplate.query("SELECT * FROM members ", MEMBER_ROW_MAPPER);
    }

    @Override
    public Member getMemberById(Integer id) {
        return jdbcTemplate.queryForObject("SELECT * FROM members WHERE id = ?",
                new BeanPropertyRowMapper<>(Member.class),
                id);
    }
}
