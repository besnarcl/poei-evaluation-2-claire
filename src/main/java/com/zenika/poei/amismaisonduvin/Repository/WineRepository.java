package com.zenika.poei.amismaisonduvin.Repository;

import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.domain.Wine;
import org.springframework.boot.web.context.WebServerInitializedEvent;

import java.util.List;
import java.util.Set;

public interface WineRepository {

    // Create and Update a bottle of wine
    Wine save(Wine wine);

    // Get all wines
    Set<Wine> getWines();

    // Get a bottle of wine by its id
    Wine getWineById(Integer id);

    // Get list of wines from region
    List<Wine> getWineFromRegion (String region);

    // Delete wine
    void delete(Wine wine);
}
