package com.zenika.poei.amismaisonduvin.Repository;

import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.domain.Review;
import com.zenika.poei.amismaisonduvin.domain.Wine;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@Primary
public class JdbcWineRepository implements WineRepository {

    public static final RowMapper<Wine> WINE_ROW_MAPPER = (rs, i) -> new Wine(
            rs.getInt("id"),
            rs.getString("appellation"),
            rs.getInt("vintage"),
            rs.getString("region"),
            rs.getString("colour"),
            rs.getString("variety"));

    public static final RowMapper<Review> REVIEW_ROW_MAPPER = (rs, i) -> new Review(new Member(
            rs.getInt("id"),
            rs.getString("pseudonym")),
            rs.getString("comment"),
            rs.getInt("score"));


    private final JdbcTemplate jdbcTemplate;

    public JdbcWineRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // Create new wine
    public Wine create(Wine wine) {
        return jdbcTemplate.queryForObject("INSERT INTO wine(appellation, vintage, region, colour, variety) VALUES (?,?,?,?,?) RETURNING *", WINE_ROW_MAPPER,
                wine.getAppellation(),
                wine.getVintage(),
                wine.getRegion(),
                wine.getColour(),
                wine.getVariety());
    }

    public Wine update(Wine wine) {
        // comment and score erased
        jdbcTemplate.update("DELETE FROM review WHERE wine_id=?",
                wine.getId());

        // push review
        for (Review rv : wine.getReviews()) {
            jdbcTemplate.update("INSERT INTO review(member_id, wine_id, comment, score) VALUES (?, ?, ?, ?)",
                    rv.getMember().getId(),
                    wine.getId(),
                    rv.getComment(),
                    rv.getScore());
        }

        // updating wine
        jdbcTemplate.update("UPDATE wine SET appellation=?, vintage=?, region=?, colour=?, variety=? WHERE id=?",
                wine.getAppellation(),
                wine.getVintage(),
                wine.getRegion(),
                wine.getColour(),
                wine.getVariety(),
                wine.getId());
        return wine;
    }

    @Override
    public Wine save(Wine wine) {
        if (wine.getId() == null) {
            return create(wine);
        } else {
            return update(wine);
        }
    }

    @Override
    public Set<Wine> getWines() {
        return new HashSet<>(jdbcTemplate.query("SELECT * FROM wine", WINE_ROW_MAPPER));
    }


    @Override
    public Wine getWineById(Integer id) {

        Set<Review> reviews = new HashSet<>(jdbcTemplate.query("SELECT * FROM wine LEFT JOIN review r on wine.id = r.wine_id LEFT JOIN members m on r.member_id = m.id AND r.wine_id = wine_id WHERE wine_id = ?",
                REVIEW_ROW_MAPPER,
                id));

        // find wine
        Wine wine = jdbcTemplate.queryForObject("SELECT * FROM wine WHERE id = ?",
                new BeanPropertyRowMapper<>(Wine.class), id);

        // Add review to wine then return wine
        wine.setReviews(reviews);

        return wine;
    }

    @Override
    public List<Wine> getWineFromRegion(String region) {

        Set<Review> reviews = new HashSet<>(jdbcTemplate.query("SELECT * FROM wine LEFT JOIN review r on wine.id = r.wine_id LEFT JOIN members m on r.member_id = m.id AND r.wine_id = wine_id WHERE wine_id = ?",
                REVIEW_ROW_MAPPER,
                region));

        // find wine
        Wine wine = jdbcTemplate.queryForObject("SELECT * FROM wine WHERE region = ?",
                new BeanPropertyRowMapper<>(Wine.class), region);

        // Add review to wine then return wine
        wine.setReviews(reviews);

        return List.of(wine);
    }

    @Override
    public void delete(Wine wine) {
        //Delete reviews first
        jdbcTemplate.update("DELETE FROM review WHERE cart_id = ?", wine.getId());


        // Delete wine after
        jdbcTemplate.update("DELETE FROM wine WHERE id = ?",wine.getId());
    }
}
