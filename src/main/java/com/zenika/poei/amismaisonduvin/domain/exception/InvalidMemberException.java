package com.zenika.poei.amismaisonduvin.domain.exception;

public class InvalidMemberException extends FunctionalException {

    public InvalidMemberException() {
    }

    public InvalidMemberException(String message) {
        super(message);
    }

    public InvalidMemberException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidMemberException(Throwable cause) {
        super(cause);
    }

    public InvalidMemberException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
