package com.zenika.poei.amismaisonduvin.domain;

import com.zenika.poei.amismaisonduvin.domain.exception.InvalidMemberException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class Member {

    private Integer id;
    private String firstName;
    private String lastName;
    private String pseudonym;

    private static final Logger LOGGER = LoggerFactory.getLogger(Member.class);



    // appear message in console when Springboot runs
    public Member() {
        LOGGER.info("Member created");
    }

    //Constructor


    public Member(Integer id, String pseudonym) {
        this.id = id;
        this.pseudonym = pseudonym;
    }

    public Member(Integer id, String firstname, String lastname, String pseudonym) {
        this.id = id;
        this.firstName = firstname;
        this.lastName = lastname;
        this.pseudonym = pseudonym;
    }


    // Getters and Setters
    public Integer getId() {
        return id;
    }

    public Member setId(Integer id) {
        this.id = id;

        return this;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public Member setPseudonym(String pseudonym) {
        if (!StringUtils.hasText(pseudonym)) {
            throw new InvalidMemberException("Pseudonym is empty");
        }

        this.pseudonym = pseudonym;

        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Member setFirstName(String firstName) {
        if (!StringUtils.hasText(firstName)) {
            throw new InvalidMemberException("FirstName is empty");
        }

        this.firstName = firstName;

        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Member setLastName(String lastName) {
        if (!StringUtils.hasText(lastName)) {
            throw new InvalidMemberException("LastName is empty");
        }

        this.lastName = lastName;

        return this;
    }
}
