package com.zenika.poei.amismaisonduvin.domain;

import com.zenika.poei.amismaisonduvin.domain.exception.InvalidReviewException;
import com.zenika.poei.amismaisonduvin.domain.exception.InvalidWineException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.Year;
import java.util.*;

@Component
public class Wine {

    private Integer id;
    private String appellation;
    private String region;
    private int vintage;
    private String colour;

    private Set<Review> reviews = new HashSet<>();

    private static final Logger LOGGER = LoggerFactory.getLogger(Wine.class);



    // appear message in console when Springboot runs
    public Wine() {
        LOGGER.info("Wine created");
    }


    public Wine(String appellation, int vintage, String region, String colour, String variety) {
        this(null, appellation, vintage, region, colour, variety, new HashSet<>());
    }

    public Wine(Integer id, String appellation, int vintage, String region, String colour, String variety, Set<Review> reviews) {
        setId(id);
        setAppellation(appellation);
        setRegion(region);
        setVintage(vintage);
        setColour(colour);
        setReviews(reviews);
        setVariety(variety);
    }

    /**
     * It is the grape variety :
     */
    private String variety;

    public Wine(int id, String appellation, int vintage, String region, String colour, String variety) {
        this.id = id;
        this.appellation = appellation;
        this.vintage = vintage;
        this.region = region;
        this.colour = colour;
        this.variety = variety;
    }

    public Integer getId() {
        return id;
    }

    public Wine setId(Integer id) {
        this.id = id;

        return this;
    }

    public String getAppellation() {
        return appellation;
    }

    public Wine setAppellation(String appellation) {
        if (!StringUtils.hasText(appellation)) {
            throw new InvalidWineException("Appellation is empty");
        }

        this.appellation = appellation;

        return this;
    }

    public String getRegion() {
        return region;
    }

    public Wine setRegion(String region) {
        if (!StringUtils.hasText(region)) {
            throw new InvalidWineException("Region is empty");
        }

        this.region = region;

        return this;
    }

    public int getVintage() {
        return vintage;
    }

    public Wine setVintage(int vintage) {
        int year = Calendar.getInstance().get(Calendar.YEAR);

        if(year < vintage){
            throw new InvalidWineException("Vintage " + vintage + " is in future");
        }

        this.vintage = vintage;

        return this;
    }

    public String getColour() {
        return colour;
    }

    public Wine setColour(String colour) {
        if (!StringUtils.hasText(colour)) {
            throw new InvalidWineException("Colour is empty");
        }

        this.colour = colour;

        return this;
    }

    public String getVariety() {
        return variety;
    }

    public Wine setVariety(String variety) {
        if (!StringUtils.hasText(variety)) {
            throw new InvalidWineException("Variety is empty");
        }

        this.variety = variety;

        return this;
    }

    public Set<Review> getReviews() {
        return new HashSet<>(reviews);
    }

    public void setReviews(Set<Review> reviews) {
        if (reviews == null) {
            throw new InvalidWineException("Reviews is undefined");
        }

        this.reviews = reviews;
    }

    //TODO à finir
    public void addReview(Review review) {
        if (review == null) {
            throw new InvalidReviewException("Review is undefined");
        }

        reviews.add(review);
    }
}
