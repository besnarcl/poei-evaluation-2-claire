package com.zenika.poei.amismaisonduvin.domain;

import com.zenika.poei.amismaisonduvin.domain.exception.InvalidReviewException;

public class Review {

    private Member member;
    private int score;
    private String comment;

    public Review(Member member, String comment, int score) {
        setMember(member);
        setComment(comment);
        setScore(score);
    }

    public Member getMember() {
        return member;
    }

    public Review setMember(Member member) {
        if (member == null) {
            throw new InvalidReviewException("Member is undefined");
        }

        this.member = member;

        return this;
    }

    public int getScore() {
        return score;
    }

    public Review setScore(int score) {
        if (score <0 || score > 5) {
            throw new InvalidReviewException("Score must be between 0 and 5");
        }

        this.score = score;

        return this;
    }

    public String getComment() {
        return comment;
    }

    public Review setComment(String comment) {
        this.comment = comment;

        return this;
    }
}
