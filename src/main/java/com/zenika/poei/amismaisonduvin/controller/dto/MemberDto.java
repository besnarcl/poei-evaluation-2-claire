package com.zenika.poei.amismaisonduvin.controller.dto;

import com.zenika.poei.amismaisonduvin.domain.Member;

public class MemberDto {

    private String firstName;
    private String lastName;
    private String pseudonym;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym;
    }

    public static MemberDto fromDomain(Member member){

        MemberDto memberDto = new MemberDto();
        memberDto.setFirstName(member.getFirstName());
        memberDto.setLastName(member.getLastName());
        memberDto.setPseudonym(member.getPseudonym());

        return memberDto;
    }

}
