package com.zenika.poei.amismaisonduvin.controller.dto;

import com.zenika.poei.amismaisonduvin.domain.Review;
import com.zenika.poei.amismaisonduvin.domain.Wine;

import java.util.HashSet;
import java.util.Set;

public class WineDto {


    private Integer id;
    private String appellation;
    private String region;
    private int vintage;
    private String colour;
    private String variety;

    private Set<Review> reviews = new HashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppellation() {
        return appellation;
    }

    public void setAppellation(String appellation) {
        this.appellation = appellation;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getVintage() {
        return vintage;
    }

    public void setVintage(int vintage) {
        this.vintage = vintage;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    public static WineDto fromDomain(Wine wine){

        WineDto wineDto = new WineDto();
        wineDto.setId(wine.getId());
        wineDto.setAppellation(wine.getAppellation());
        wineDto.setVintage(wine.getVintage());
        wineDto.setRegion(wine.getRegion());
        wineDto.setColour(wine.getColour());
        wineDto.setVariety(wine.getVariety());
        wineDto.setReviews(wine.getReviews());

        return wineDto;
    }
}

