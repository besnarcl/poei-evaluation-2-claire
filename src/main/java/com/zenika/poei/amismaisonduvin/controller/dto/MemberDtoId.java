package com.zenika.poei.amismaisonduvin.controller.dto;

import com.zenika.poei.amismaisonduvin.domain.Member;

public class MemberDtoId {

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public static MemberDtoId fromDomain(Member member ){

        MemberDtoId memberDtoId = new MemberDtoId();
        memberDtoId.setId(member.getId());

        return memberDtoId;
    }
}
