package com.zenika.poei.amismaisonduvin.controller;

import com.zenika.poei.amismaisonduvin.Repository.MemberRepository;
import com.zenika.poei.amismaisonduvin.controller.dto.MemberDto;
import com.zenika.poei.amismaisonduvin.controller.dto.MemberDtoId;
import com.zenika.poei.amismaisonduvin.controller.dto.MemberInputDtoFirstName;
import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.domain.exception.InvalidMemberException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class MemberController {

    private MemberRepository memberRepository;

    // Constructor
    public MemberController(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    // create
    @PostMapping("/members")
    public ResponseEntity<Member> addMember(@RequestBody Member member) {
        // create a new member
        Member newMember = memberRepository.save(member);

        return ResponseEntity
                .created(URI.create("/members/" + newMember.getPseudonym()))
                .body(newMember);
    }

    @GetMapping("/members")
    public Set<MemberDto> getMembers() {
        return memberRepository.getMembers().stream()
                .map(MemberDto::fromDomain).collect(Collectors.toSet());
    }

    //TODO enlever l'id de la réponse
    @GetMapping("/members/{id}")
    public Member getMemberById(@PathVariable("id") int id) {

        Member member = memberRepository.getMemberById(id);

        // if cart exist -> return cart else throws an exception
        if (member != null) {
            return member;
        } else {
            throw new InvalidMemberException(String.valueOf(id));
        }
    }

    //update
    //TODO:  erreur 500 aucun résultat retourné par la requête
    @PutMapping("/members/{id}")
    public ResponseEntity<MemberDto> updateMember(@PathVariable ("id") Integer id, @RequestBody MemberInputDtoFirstName inputDtoFirstName){

        //find member to update
        Member memberToUpdate = memberRepository.getMemberById(id);

        //Set firstname of member to update
        memberToUpdate.setFirstName(inputDtoFirstName.getFirstName());

        //update product
        memberRepository.save(memberToUpdate);

        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .location(URI.create("/members/" + memberToUpdate.getId()))
                .body(MemberDto.fromDomain(memberToUpdate));
    }



}
