package com.zenika.poei.amismaisonduvin.controller.dto;

public class MemberInputDtoFirstName {

    private String firstName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
