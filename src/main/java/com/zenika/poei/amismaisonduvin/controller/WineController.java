package com.zenika.poei.amismaisonduvin.controller;

import com.zenika.poei.amismaisonduvin.Repository.MemberRepository;
import com.zenika.poei.amismaisonduvin.Repository.WineRepository;
import com.zenika.poei.amismaisonduvin.controller.dto.MemberDto;
import com.zenika.poei.amismaisonduvin.controller.dto.WineDto;
import com.zenika.poei.amismaisonduvin.domain.Wine;
import com.zenika.poei.amismaisonduvin.domain.exception.InvalidWineException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class WineController {

    private WineRepository wineRepository;
    private MemberRepository memberRepository;


    //Constructor
    public WineController(WineRepository wineRepository, MemberRepository memberRepository) {
        this.wineRepository = wineRepository;
        this.memberRepository = memberRepository;
    }

    //Create
    @PostMapping("/wines")
    public ResponseEntity<Wine> addWine(@RequestBody Wine wine) {
        //create wine
        Wine newWine = wineRepository.save(wine);

        return ResponseEntity
                .created(URI.create("/wines/" + newWine.getId()))
                .body(newWine);
    }

    //get all wines
    @GetMapping("/wines/")
    public Set<WineDto> getAll(){
        return wineRepository.getWines().stream()
                .map(WineDto::fromDomain).collect(Collectors.toSet());
    }

    /*Récupération d’une bouteille par id
    GET /wines/123

     Réponse
    {“id”: 123, “appellation”: “Roche Mazet”, “colour” : … }
     */
    @GetMapping("/wines/{id}")
    public Wine getById(@PathVariable("id") int id){
        Wine wine = wineRepository.getWineById(id);

        // if wine exist -> return wine else throws an exception
        if (wine != null) {
            return wine;
        } else {
            throw new InvalidWineException(String.valueOf(id));
        }
    }


    // TODO : get wines from region -> URL identique à getAll() -> pas assez de temps pour y réfléchir
/*Récupération des bouteilles d’une region
    GET /wines?region=Bordelais


    @GetMapping("/wines/")
    public Set<WineDto> getByRegion(@RequestParam ("region") String region, @RequestBody Wine wine){

        return
    }*/

    // TODO : Updating of bottle -> méthode non finie pas assez de temps

/*
Mise à jour d’une bouteille
    PUT /wines/123
    {“appellation”: “Roche-Mazet”, “colour” : … }

    Réponse
    {“id”: 123, “appellation”: “Roche-Mazet”, “colour” : … }

    @PutMapping("/wines/{id}")
    public ResponseEntity<WineDto> updateBottle(@PathVariable ("id") int id, @RequestParam Wine wine){

        // find bottle
        wineRepository.getWineById(id);

        // updating bottle




        return ResponseEntity.status(HttpStatus.ACCEPTED)
                .location(URI.create("/wines/" + wine.getId()))
                .body(WineDto.fromDomain(wine));
    }*/

/*Suppression d’une bouteille
    DELETE /wines/123

    Supprimer les avis associés à cette bouteille.
*/
}
